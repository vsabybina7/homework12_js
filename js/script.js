// Задание
// Реализовать программу, показывающую циклично разные картинки.
//
//     Технические требования:
//
//     В папке banners лежит HTML код и папка с картинками.
//     При запуске программы на экране должна отображаться первая картинка.
//     Через 10 секунд вместо нее должна быть показана вторая картинка.
//     Еще через 10 секунд - третья.
//     Еще через 10 секунд - четвертая.
//     После того, как покажутся все картинки - этот цикл должен начаться заново.
//     При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
//     По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
//     Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

let a = 0;
let imageColection = document.body.querySelectorAll(".image-to-show");

const changeImg = () => {
    imageColection.style = "display: flex";

    imageColection.forEach((item, i) => {
        if (i!== a % imageColection.length){
            item.style ="opacity: 0; order: 1"
        } else {
            item.style = "opacity: 1; order: 0";
        }
    });
    a++
};
    let timerId = setInterval(changeImg, 1000);

    let butt = document.getElementById('btn');

    butt.addEventListener('click', ()=> {
        clearInterval(timerId);
    });

    let input = document.getElementById('inp');


    input.addEventListener('click', ()=> {
        clearInterval(timerId);
        timerId = setInterval(changeImg, 1000);
    });